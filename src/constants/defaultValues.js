export const defaultMenuType = 'menu-default'; //'menu-sub-hidden', 'menu-hidden'
export const defaultStartPath = '/dashboard';
export const subHiddenBreakpoint = 1440;
export const menuHiddenBreakpoint = 768;
export const defaultLocale = {
	locale: {
		languageId: 'english',
		locale: 'en',
		name: 'English',
		icon: 'en'
	}
}
export const apiUrl = ""
export const searchPath = "/search"
