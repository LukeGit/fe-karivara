
import React from 'react';
import 'antd/dist/antd.css';
import { Drawer, Icon } from 'antd';
import './index.scss';
import {notificationService} from  '../../service/notification';


var TxtType = function (el, toRotate, period) {
  this.toRotate = toRotate;
  this.el = el;
  this.loopNum = 0;
  this.period = parseInt(period, 10) || 2000;
  this.txt = '';
  this.tick();
  this.isDeleting = false;
};

TxtType.prototype.tick = function () {
  var i = this.loopNum % this.toRotate.length;
  var fullTxt = this.toRotate[i];

  if (this.isDeleting) {
    this.txt = fullTxt.substring(0, this.txt.length - 1);
  } else {
    this.txt = fullTxt.substring(0, this.txt.length + 1);
  }

  this.el.innerHTML = '<span class="wrap">' + this.txt + '</span>';

  var that = this;
  var delta = 200 - Math.random() * 100;

  if (this.isDeleting) { delta /= 2; }

  if (!this.isDeleting && this.txt === fullTxt) {
    delta = this.period;
    this.isDeleting = true;
  } else if (this.isDeleting && this.txt === '') {
    this.isDeleting = false;
    this.loopNum++;
    delta = 500;
  }

  setTimeout(function () {
    that.tick();
  }, delta);
};

class App extends React.Component {
  state = {
     visible: false,
     email: '',
     content: '',
   };

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };
  componentDidMount(){
    var elements = document.getElementsByClassName('typewrite');
    for (var i = 0; i < elements.length; i++) {
      var toRotate = elements[i].getAttribute('data-type');
      var period = elements[i].getAttribute('data-period');
      if (toRotate) {
        new TxtType(elements[i], JSON.parse(toRotate), period);
      }
    }
  }

  onChange=(e)=>{
    console.log(e.target.id)
    if(e.target.id ==='email'){
      this.setState({
        email: e.target.value,
      })
    }
    else{
      this.setState({
        content: e.target.value,
      })
    }
  }
  handleSubmit=(e)=>{
    let {email,content} = this.state;
    if(!email || !content){
      notificationService.error('please Input');
      e.preventDefault();
    }
  }
  render() {
    return (
      <div className="homepage">
      <div className="container">
        <div className="top-bar">
          <p className="top-bar__company-name">Karivara</p>
          <button className="top-bar__btn-toggle-form btn" onClick={()=>this.showDrawer()}>Get in touch</button>
       
        </div>
        <div className="homepage__main-content">
          <div className="slogan-box">
            <p>We</p>
            <p href="" className="typewrite" data-period="2000" data-type='[ "Build Products.", "Launch Ventures.", 
            "Invest in Ideas."]'>
            </p>
          </div>
          <div className="description">
            <p>
              Karivara is a product and innovation studio. We are known for what we love.
              </p>

          </div>
        </div>
        <div className="divider">
        </div>
        <Drawer
          placement="right"
          closable={false}
          onClose={this.onClose}
          visible={this.state.visible}
          className="form"
          width={'40%'}
        >
         <form className="form" method="POST" action="https://formspree.io/support@karivara.com">
            <div className="form__close-btn" onClick={()=>this.onClose()}>
            <Icon type="close" />
            </div>
              <h1>
              Want to know the secret? Get in touch with us!
              </h1>
              <div className="form-group">
                <label>What is your email?</label>
                <input name="email"  value={this.state.email} id="email" 
                placeholder="Enter your email address" onChange={(e)=>this.onChange(e)}/>
              </div>
              <div className="form-group">
                <label>What do you want to tell us?</label>
                <textarea name="message" value={this.state.content} id="content" placeholder="Enter the messages you want to convey to us." onChange={(e)=>this.onChange(e)}/>
              </div>
              <button className="btn-submit btn" type="submit" onClick={this.handleSubmit}><p>SEND</p></button>
          </form>
        </Drawer>
      </div>
    </div>
    
    );
  }
}

        
export default App; 