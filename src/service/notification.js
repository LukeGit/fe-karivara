import { notification } from 'antd';

class NotificationService {
  error(message, title, options) {
    notification['error']({
      message: title || 'Error',
      description: message,
      ...options,
    });
  }
  success(message, title, options) {
    notification['success']({
      message: title || 'Success',
      description: message,
      ...options,
    });
  }
}

export const notificationService = new NotificationService();
