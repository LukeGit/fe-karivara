import React, {Component} from 'react';
import {Route, Switch, Redirect} from 'react-router-dom';
import Home from '../../pages/authentication/homepage/home';

class HomeRouter extends Component{
    render(){
        return(
            <Switch>
                <Route path="/homepage" component={Home} />
                <Redirect to="/homepage" />
            </Switch>
        )
    }
}
export default HomeRouter;