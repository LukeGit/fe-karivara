import React, { Component } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import AppRouter from "./app";
import 'antd/dist/antd.css';
import "Assets/css/vendor/bootstrap.min.css";
import "react-perfect-scrollbar/dist/css/styles.css";
import "Assets/css/sass/themes/gogo.light.purple.scss";
import { setupAxios } from '../services/requester';
import Login from "../pages/authentication/login/login";

class MainRouter extends Component {
  constructor(props) {
    super(props);
    setupAxios();
  }
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/app" component={AppRouter} />
          <Redirect to="/app" />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default MainRouter;
