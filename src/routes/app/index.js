import React, { Component } from "react";
import { Route, Switch, } from "react-router-dom";
import HomePage from "Containers/HomePage";
import { defaultMenuType } from 'Constants/defaultValues'
class AppRouter extends Component {

  render() {
    return (
      <div id="app-container" className={defaultMenuType} >
        <TopNav history={this.props.history} />
        <Switch>
          <Route path={`${match.url}/home`} component={HomePage} />
        </Switch>
      </div>
    );
  }
}
export default AppRouter

